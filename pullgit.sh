#!/bin/bash

echo "suckless"
cd /home/ben/opt/suckless/
git pull

echo "mutt"
cd /home/ben/.mutt/
git pull

echo "scripts"
cd /home/ben/scripts/
git pull

echo "francis"
cd /home/ben/keepass/francis/
git pull

echo "kk"
cd /home/ben/keepass/kk/
git pull

echo "jj"
cd /home/ben/keepass/jj/
git pull

echo "ryan"
cd /home/ben/keepass/ryan/
git pull

echo "dotfiles"
cd /home/ben/.dotfiles/
git pull

echo "ansible-basic"
cd /home/ben/workspace/ansible-basics
git pull

echo "Done"
