#!/usr/bin/env bash
# fix-omni.sh
# must be run as root
# This is a script to change the context menu of Copy Link to Copy Link Location
# steps can be found here: https://bugzilla.mozilla.org/show_bug.cgi?id=1701324#c59

omnija="browser/omni.ja"

if [ $# -eq 0 ] || [ -z "$1" ] ; then
	printf 'Usage: %s FirefoxInstallationDirectory\n' "$0" >&2
	exit 1
elif ! [ -d "$1" ] ; then
	printf 'Firefox installation directory "%s" does not exist\n' "$1" >&2
	exit 2
elif ! [ -f "$1/$omnija" ] ; then
	printf 'File %s/%s does not exist\n' "$1/$omnija" >&2
	exit 3
elif ! which zip &>/dev/null ; then
	printf 'Please, install zip\n' >&2
	exit 4
elif ! which unzip &>/dev/null ; then
	printf 'Please, install unzip\n' >&2
	exit 4
else
	omnijadir="${omnija%/*}"
	omnijafile="${omnija##*/}"
	(
		cd "$1/$omnijadir"
		unzip -qq -d "$omnijafile.dir" "$omnijafile"
		mv "$omnijafile" "$omnijafile.$( date +%Y%m%d-%H%M%S )"
		cd "$omnijafile.dir"
		perl -i -pe '$k="A" if /\.label\s+=\s+Copy\s+Email\s+Address/i; $k="a" if s/\.label\s+=\s+Copy\s+Link$/$& (URL Address)/i; s/(\.accesskey\s+=\s+)\S/$1$k/,undef($k) if /\.accesskey\s+=\s+/ and defined($k) and $k ne "";' localization/en-US/browser/browserContext.ftl
		zip -q0DXr ../"$omnijafile" .
		cd ..
		rm -rf "$omnijafile.dir"
	)
fi
