#!/bin/sh

for i in `ls *.jpg`;
do
    mv $i "${i%.jpg}"_result-`date +%m-%d-%Y`.jpg;
done
