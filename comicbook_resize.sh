#!/bin/bash

# usage: resize_comicbook <total-number-of-zip-files> <comicbook-name> <author-name>
# only works for zip files that starts with vol_ then number of the volume:
# e.g. vol_298-English.zip

_total=$(ls -l *.zip | wc -l)
comicbookname=$1
authors=$2

create_book() {
    check_number_of_contents=$( ls "${comicbookname}"/*.{png,jpeg} 2> /dev/null | wc -l )
    newfilename=${comicbookname}-${appendname}

    # make sure files are in proper order with the use of ls command
    # specifying {png,jpeg} will prioritize png before jpeg, use *.* instead
    zip "$newfilename".cbr $(ls -1 "$comicbookname"/*.*)
    calibredb add "$newfilename".cbr
    # cbr can't add or modify author using cli, need to convert to epub

    # echo "converting to epub"
    # ebook-convert "$newfilename".cbr "$newfilename".epub -vv --cover="$cover" --authors="$authors" # --series="$comicbookname"
    # calibredb add "$newfilename".epub

    # clean up
    # rm -rf "$comicbookname" *.{cbr,epub}

    # sleep for 3 mins so cpu will stop getting hot

    # clean up
    rm -rf "$comicbookname"
    # sleep 180s
}

show_usage() {
    echo "Usage:"
    echo "comicbook_resize.sh <comicbook-name> <author-name>"
}

if [[ -z $1 || -z $2  ]];
then
    show_usage;
    exit 1;
fi

# echo $_total

cover=""
appendname=""

# for debugging
# echo $( eval echo "vol_${_start}*.zip" );

count=1
total_count=1

for i in *.zip;
do
    # create comicbookname folder if not present
    test -d $comicbookname || mkdir -p $comicbookname
    foldername="${i%.*}";

    # echo "Current count: $count"
    echo "Zip file: $i";
    echo "Folder name: $foldername";
    # exit 1

    test -n "$appendname" || appendname="$foldername"
    unzip -d $foldername $i;

    cd $foldername

    # remove files that're not images
    ls | egrep -v '(png|jpeg)' | xargs rm

    # specifying file extension will not use proper order
    # this will result to a bug when creating a cover (e.g. a different image will be used as a cover even if it's not the initial image)
    # so use wildcard instead
    for image in *.*;
    do
        # echo "converting $image to ${foldername}_${image}";
        # convert "$image" -resize 1072x1448\! "${foldername}_${image}";

        echo "moving $image to ${foldername}_${image}";
        mv "$image" "${foldername}_${image}";

        test -n "$cover" || cover="${comicbookname}/${foldername}_${image}";

        echo "moving ${foldername}_${image} to $comicbookname";
        mv "${foldername}_${image}" -t ../"$comicbookname";
    done

    cd -

    rm -rf $foldername

    # debugging:
    echo "Current count: $count"
    echo "Current total: $_total"
    echo "Current total_count: $total_count"

    if [[ $count -eq 10 || $total_count -eq $_total ]];
    then
        create_book

        # reset necessary variables for the next loop
        count=1
        cover=""
        appendname=""

        # sleep 120s
    else
        count=$(($count+1))
    fi

    # echo "test here"

    total_count=$(($total_count+1))
done

# clean up
# rm -rf "$comicbookname"

echo "All done :)"
